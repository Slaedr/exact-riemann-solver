#! /usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt

p = np.genfromtxt("solution.txt")

plt.plot(p[:,0],p[:,1],label="Density")
plt.plot(p[:,0],p[:,2],label="Velocity")
plt.plot(p[:,0],p[:,3],label="Pressure")
plt.xlabel("x")
plt.legend()
plt.grid(True)
plt.title("Sod shock tube, t = 0.2 s")
plt.show()
