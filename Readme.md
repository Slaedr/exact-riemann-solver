Exact Riemann Solver
====================

Requires Boost.
Run `make` to build, `./riemann.out` to run and `./plots.py` or `python3 plots.py` to plot the solution.

The `main()` function is configured to solve the Sod shock tube problem, and gives the solution at t=0.2 seconds after the start of the simulation.
