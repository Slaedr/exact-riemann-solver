riemann:
	$(CXX) -o riemann.out -std=c++14 -Wall -Werror -O2 riemann.cpp

clean:
	rm *.out

all:
	riemann
