/** \file ariemann.hpp
 * \author Aditya Kashi
 *
 * Verified against  Section 4.3.3 in 
 * E.F. Toro, "Riemann solvers and numerical methods for fluid dynamics", 3rd ed., 2009.
 */

#ifndef AEXACTRIEMANN_H
#define AEXACTRIEMANN_H

#include <cmath>
#include <iostream>
#include <fstream>

#include <boost/multi_array.hpp>

namespace acfd {

/// Solve a 1D Riemann problem `exactly'
/** The regions in the space, w.r.t. the solution are designated as follows.
 * - R is the undisturbed right state
 * - 2 is the state between the shock and the contact
 * - 3 is the state between the contact and the start (or the `foot') of the expansion fan
 * - 4 is the state within the expansion wave fan
 * - L is the undisturbed left state.
 */
class RiemannProblem
{
	const double pr;
	const double pl;
	const double rhor;
	const double rhol;
	const double vr;
	const double vl;
	
	const double x0;		///< initial position of discontinuity
	
	const double gamma;		///< adiabatic index c_p/c_v
	
	const double cl;
	const double cr;
	
	double p2;
	double rho2;
	double v2;
	
	double rho3;
	
public:
	/** Arguments in order: right pressure, left pressure, right density, left density, right velocity, left velocity, 
	 * initial position of discontinuity, adiabatic index
	 */
	RiemannProblem(double prr, double pll, double rhorr, double rholl, double vrr, double vll, double xl, double g)
		: pr{prr}, pl{pll}, rhor{rhorr}, rhol{rholl}, vr{vrr}, vl{vll}, x0{xl}, gamma{g},
		  cl{std::sqrt(gamma*pl/rhol)}, cr{std::sqrt(gamma*pr/rhor)}
	{ }

	/// Evaluates the nonlinear function of the pressure ratio p2/pR across the shock that we equate to zero
	/** \param P Pressure ratio p2/pR across the shock
	 */
	double f(const double P) const
	{
		return P*std::pow(1+(gamma-1)/(2*cl)*(vl-vr-cr/gamma*(P-1)/std::sqrt((gamma+1)/(2*gamma)*(P-1)+1)), 
				-2*gamma/(gamma-1)) - pl/pr;
	}
	
	/// Derivative of the function \ref f w.r.t. the pressure ratio p2/pR
	double df(const double P) const
	{
		using std::sqrt;
		using std::pow;
		double ret;
		ret = pow(1+(gamma-1)/(2*cl)*(vl-vr-cr/gamma*(P-1)/sqrt((gamma+1)/(2*gamma)*(P-1)+1)), -2*gamma/(gamma-1)) 
			- 2*gamma/(gamma-1)
			*P*pow(1+(gamma-1)/(2*cl)*(vl-vr-cr/gamma*(P-1)/sqrt((gamma+1)/(2*gamma)*(P-1)+1)), (1-3*gamma)/(gamma-1)) 
			* (-1)*(gamma-1)/(2*cl)*cr/gamma
			*(1/sqrt((gamma+1)/(2*gamma)*(P-1)+1)-(gamma+1)/(2*gamma)*(P-1)/2*1/pow((gamma+1)/(2*gamma)*(P-1)+1, 1.5));
		return ret;
	}
	
	/// Solve the nonlinear equation for the pressure ratio across the shock by Newton-Raphson method
	void solve_NR(const double tol)
	{
		std::cout << "RiemannSolver: solve_NR: Starting Newton-Raphson iterations\n";
		// note that x = p2/pr
		double x = (pl+pr)/(2*pr);
		int i = 0;
		double xold = (pl+pr)/(2*pr);
		do
		{	
			xold = x;
			x = xold - f(xold)/df(xold);
			i++;
		} while(std::fabs(x-xold) >= tol);
		
		std::cout << "RiemannSolver: solve_NR: Newton-Raphson iterations completed in " << i << " iterations.\n";
		std::cout << "ariemann: solve_NR: p2/pr is " << x << std::endl;
		
		// region 2:
		p2 = x*pr;
		v2 = vr + cr/gamma*(x-1)/std::sqrt((gamma+1)/(2*gamma)*(x-1)+1);
		rho2 = rhor * (1+(gamma+1)/(gamma-1)*x) / ((gamma+1)/(gamma-1) + x);
		
		// region 3: note that p3 = p2, and v3 = v2
		rho3 = rhol* std::pow(p2/pl, 1/gamma);
	}
	
	/// Velocity in the expansion fan
	double v4(const double x, const double t) const
	{ return 2/(gamma+1)*((x-x0)/t + cl + (gamma-1)/2*vl); }
	
	/// Speed of sound in the expansion fan
	double c4(const double x, const double t) const
	{ return v4(x,t) - (x-x0)/t; }
	
	/// Density in the expansion fan
	double rho4(const double x, const double t) const
	{ return rhol*std::pow( c4(x,t)/cl , 2/(gamma-1)); }
	
	/// Pressure in the expansion fan
	double p4(const double x, const double t) const
	{ return pl*std::pow( c4(x,t)/cl , 2*gamma/(gamma-1)); }
	
	/// Position of shock
	double xshock(const double t) const
	{ 
		//return (vr + (p2/pr-1)*cr*cr/(gamma*(v2-vr)))*t + x0;
		
		return (rhor*vr-rho2*v2)/(rhor-rho2)*t + x0;
	}
	
	/// Position of the contact wave
	double xcontact(const double t) const {
		return v2*t + x0; 
	}
	
	/// Position of the 'foot' or the start of the expansion fan
	double xexpn_r(const double t) const {
		return (v2- std::sqrt(gamma*p2/rho3))*t + x0;	// since v3 = v2 and p3 = p2
	}
	
	/// Position of the 'head' or the end of the expansion fan
	double xexpn_l(const double t) const {
		return (vl-cl)*t + x0; 
	}
	
	/** Writes out the x-coordinate, density, velocity and pressure in that order.
	 * 
	 * \param t time instant for which to export data
	 * \param xstart start of x-range to output data in
	 * \param xend end of xrange
	 * \param N number of x-points
	 * \param[in,out] fv An IO stream to write the output to.
	 */
	void export_data(const double t, const double xstart, const double xend, const int N, std::ofstream& fv) const
	{
		// The columns in order are: x, rho, v, p
		boost::multi_array<double,2> d(boost::extents[N][4]);
		//typedef array_type::index index;

		double delta = (xend-xstart)/(N-1.0);
		for(int i = 0; i < N; i++)
			d[i][0] = xstart + i*delta;

		const double shockpos = xshock(t);
		const double contpos = xcontact(t);
		const double xexpn_lpos = xexpn_l(t);
		const double xexpn_rpos = xexpn_r(t);
		
		for(int i = 0; i < N; i++)
		{
			if( d[i][0] >= shockpos )
			{
				d[i][1] = rhor;
				d[i][3] = pr;
				d[i][2] = vr;
			}
			else if( d[i][0] >= contpos )
			{
				d[i][1] = rho2;
				d[i][3] = p2;
				d[i][2] = v2;
			}
			else if( d[i][0] >= xexpn_rpos)
			{
				d[i][1] = rho3;
				d[i][3] = p2;
				d[i][2] = v2;
			}
			else if( d[i][0] >= xexpn_lpos )
			{
				d[i][1] = rho4(d[i][0],t);
				d[i][3] = p4(d[i][0],t);
				d[i][2] = v4(d[i][0],t);
			}
			else
			{
				d[i][1] = rhol;
				d[i][3] = pl;
				d[i][2] = vl;
			}
		}

		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < 4; j++)
				fv << d[i][j] << "   ";
			fv << '\n';
		}
	}
	
	/// Returns density at position of initial discontinuity
	double mid_density(double t) const
	{
		double d;
		if( x0 >= xshock(t) )
		{
			d = rhor;
		}
		else if( x0 >= xcontact(t) )
		{
			d = rho2;
		}
		else if( x0 >= xexpn_r(t) )
		{
			d = rho3;
		}
		else if( x0 >= xexpn_l(t) )
		{
			d = rho4(x0,t);
		}
		else
		{
			d = rhol;
		}
		return d;
	}
	
	/// Returns the velocity at the position of the initial discontinuity
	double mid_velocity(double t) const
	{
		double v;
		if( x0 >= xshock(t) )
		{
			v = vr;
		}
		else if( x0 >= xcontact(t) )
		{
			v = v2;
		}
		else if( x0 >= xexpn_r(t) )
		{
			v = v2;
		}
		else if( x0 >= xexpn_l(t) )
		{
			v = v4(x0,t);
		}
		else
		{
			v = vl;
		}
		return v;
	}
	
	/// Returns the pressure at the position of the initial discontinuity
	double mid_pressure(double t) const
	{
		double p;
		if( x0 >= xshock(t) )
		{
			p = pr;
		}
		else if( x0 >= xcontact(t) )
		{
			p = p2;
		}
		else if( x0 >= xexpn_r(t) )
		{
			p = p2;
		}
		else if( x0 >= xexpn_l(t) )
		{
			p = p4(x0,t);
		}
		else
		{
			p = pl;
		}
		return p;
	}
};

} // end namespace acfd

#endif
