#include <iomanip>
#include "ariemann.hpp"

using namespace std;
using namespace acfd;

int main(int argc, char *argv[])
{
	const double time = 0.25;
	const int N = 200;

	const double gamma = 1.4;
	const double tol = 1e-8;

	// Data for the classic Sod shock tube problem
	const double rhor = 0.125, rhol = 1.0;
	const double vr = 0.0, vl = 0.0;
	const double pr = 0.1, pl = 1.0;
	const double x0 = 0.0;

	RiemannProblem R(pr,pl, rhor,rhol, vr,vl, x0, gamma);
	R.solve_NR(tol);
	
	ofstream soln("solution.txt");
	R.export_data(time, -0.5, 0.5, N, soln);
	
	cout << setprecision(10);
	cout << "At time " << time << " seconds:\n";
	cout << "Mid density = " << R.mid_density(time) << endl;
	cout << "Mid velocity = " << R.mid_velocity(time) << endl;
	cout << "Mid pressure = " << R.mid_pressure(time) << endl;
	cout << "Shock location = " << R.xshock(time) << endl;
	cout << "Contact location = " << R.xcontact(time) << endl;
	soln.close();
	
	return 0;
}
